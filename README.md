# IDS721 Week6 miniproj

I used the lambda function of Week 5 Mini Project and instrument it with Logging and Tracing using AWS cloudwatch and X-Ray.

##  Get Start
Modify the code for week5 mini project, Build the lambda function by running `cargo lambda build --release`.
Deploy the lambda function to AWS Lambda
![](1.jpg)

Enabled the X-Ray active tracing and Lambda insight monitoring.
![](2.jpg)

Add permission to role to enable access to cloudwatch
![](3.jpg)

Conduct some tests using the build-in test tool of AWS Lambda
![](4.jpg)

View logging detail in cloudwatch group
![](5.jpg)

View tracing map in X-Ray
![](6.jpg)

