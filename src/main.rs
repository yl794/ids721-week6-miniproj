use mysql_async::{prelude::*, Pool, Row};
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde_json::{json, Value};

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let pool = Pool::new("mysql://admin:gzcgzc0721@week5.cvys4s6o2i31.us-east-1.rds.amazonaws.com:3306/week5");
    let mut conn = pool.get_conn().await?;
    let rows: Vec<Row> = conn.query("SELECT max(quantity) AS max FROM week5").await?;
    let result: Option<i64> = rows.get(0).and_then(|row| row.get("max"));

    let response = match result {
        Some(max) => json!({ "Maximum quality": max }),
        None => json!({ "Maximum quality": null }),
    };
    Ok(response)
}

